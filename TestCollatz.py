#!/usr/bin/env python3

# --------------
# TestCollatz.py
# --------------

# pylint: disable = bad-whitespace
# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz(TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 1)
        self.assertEqual(j, 10)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(34, 1987)
        self.assertEqual(v, 182)

    def test_eval_2(self):
        v = collatz_eval(28000, 34765)
        self.assertEqual(v, 311)

    def test_eval_3(self):
        v = collatz_eval(459551, 615595)
        self.assertEqual(v, 470)

    def test_eval_4(self):
        v = collatz_eval(2345, 2789)
        self.assertEqual(v, 209)

    def test_eval_5(self):
        v = collatz_eval(291, 4597)
        self.assertEqual(v, 238)

    def test_eval_6(self):
        v = collatz_eval(1941, 3103)
        self.assertEqual(v, 217)

    def test_eval_7(self):
        v = collatz_eval(995293, 370237)
        self.assertEqual(v, 525)

    def test_eval_8(self):
        v = collatz_eval(3811, 991117)
        self.assertEqual(v, 525)

    def test_eval_9(self):
        v = collatz_eval(402947, 549801)
        self.assertEqual(v, 470)

    def test_eval_10(self):
        v = collatz_eval(515694, 600234)
        self.assertEqual(v, 452)

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n"
        )


# ----
# main
# ----

if __name__ == "__main__":  # pragma: no cover
    main()
