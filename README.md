# CS373: Software Engineering Collatz Repo

* Name: Asutosh Dhakal

* EID: ad42324

* GitLab ID: asutoshdhakal

* HackerRank ID: asutosh_dhakal

* Git SHA: 5fa85c8d2fd10747e1cff9526e04bc2374b02410

* GitLab Pipelines: https://gitlab.com/asutoshdhakal/cs373-collatz/pipelines

* Estimated completion time: 5 hours

* Actual completion time: 7 hours

* Comments: The basic collatz algorithm in collatz_helper is from our discussion in class with Professor Downing.  